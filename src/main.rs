mod opt;

use structopt::StructOpt;

fn main() {
    let opt::Opt { tags, format } = opt::Opt::from_args();

    if tags.is_empty() {
        print(&["os",
            "family",
            "arch",
            "distro",
            "cpu",
            "cpuvendor",
            "cores",
            "threads",
            "mem",
            "mb",
            "mbvendor",
            "gpu",
            "gpuvendor",
            "drive",
            "drivesize"], &format);
    } else {
        print(&tags.iter().map(|a| a.as_str()).collect::<Vec<&str>>(), &format);
    }
}

fn print(tags: &[&str], format: &str) {
    match hardi::get_fields(&tags) {
        Ok(hm) => {
            match format {
                "aligned" => {
                    if tags.len() == 1 {
                        println!("{}", hm.get(tags[0]).unwrap());
                        return;
                    }
                    let mut longest_field = 0;
                    for tag in tags {
                        longest_field = tag.len().max(longest_field);
                    }
                    longest_field += 1;
                    for tag in tags {
                        print!("{}", tag);
                        for _ in tag.len()..longest_field {
                            print!(" ");
                        }
                        println!("{}", hm.get(*tag).unwrap());
                    }
                }
                "tabfile" => {
                    for tag in tags {
                        println!("{}\t{}", tag, hm.get(*tag).unwrap());
                    }
                }
                "json" => {
                    print!("{{");
                    let mut first = true;
                    for tag in tags {
                        if first {
                            first = false;
                        } else {
                            print!(", ");
                        }
                        print!("\"{}\": \"{}\"", tag, hm.get(*tag).unwrap());
                    }
                    println!("}}");
                }
                _ => {
                    eprintln!("invalid format");
                    std::process::exit(1);
                }
            }
        }
        Err(err) => {
            eprintln!("{}", err);
            std::process::exit(1);
        }
    }
}