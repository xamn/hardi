use std::process;
use std::collections::HashMap;

pub(super) fn get_field(field: &str, hm: &mut HashMap<String, String>) {
    if hm.contains_key(field) {
        return;
    }
    match field {
        "distro"      => distro(hm),
        "cpu"         |
        "cpuvendor"   |
        "cores"       |
        "threads"     |
        "mem"         => sysctl(hm),
        "mb"          |
        "mbvendor"    |
        "gpu"         |
        "gpuvendor"   |
        "drive"       |
        "drivesize"   |
        _ => panic!()
    }
}

fn distro(hm: &mut HashMap<String, String>) {
    match std::str::from_utf8(&process::Command::new("sw_vers").arg("-productVersion").output().unwrap().stdout) {
        Ok(output) => {
            hm.insert("distro".to_owned(), output.to_owned());
        }
        Err(_) => ()
    }
}

fn sysctl(hm: &mut HashMap<String, String>) {
    if hm.contains_key("cpu") {
        return;
    }
    match std::str::from_utf8(&process::Command::new("sysctl").arg("-a").output().unwrap().stdout) {
        Ok(output) => {
            for line in output.split("\n") {
                if line.starts_with("machdep.cpu.brand_string:") {
                    hm.insert("cpu".to_owned(), line["machdep.cpu.brand_string: ".len()..].to_owned());
                }
                if line.starts_with("machdep.cpu.vendor:") {
                    hm.insert("cpuvendor".to_owned(), line["machdep.cpu.vendor: ".len()..].to_owned());
                }
                if line.starts_with("machdep.cpu.core_count:") {
                    hm.insert("cores".to_owned(), line["machdep.cpu.core_count: ".len()..].trim().to_owned());
                }
                if line.starts_with("machdep.cpu.thread_count:") {
                    hm.insert("threads".to_owned(), line["machdep.cpu.thread_count: ".len()..].trim().to_owned());
                }
                if line.starts_with("hw.memsize:") {
                    hm.insert("mem".to_owned(),format!("{:.0}GB", (line["hw.memsize: ".len()..].parse::<f64>().unwrap() / 1024.0 / 1024.0 / 1024.0).ceil()));
                }
            }
        }
        Err(_) => ()
    }
}

