use std::collections::HashMap;
use std::process;

pub(super) fn get_field(field: &str, hm: &mut HashMap<String, String>) {
    if hm.contains_key(field) {
        return;
    }
    match field {
        "distro"     => { hm.insert("distro".to_owned(), get_wmic(&["os", "get", "caption"])); }
        "cpu"        => { hm.insert("cpu".to_owned(), get_wmic(&["cpu", "get", "Name"])); }
        "cpuvendor"  => { hm.insert("cpuvendor".to_owned(), get_wmic(&["cpu", "get", "Manufacturer"])); }
        "cores"      => { hm.insert("cores".to_owned(), get_wmic(&["cpu", "get", "NumberOfCores"])); }
        "threads"    => { hm.insert("threads".to_owned(), get_wmic(&["cpu", "get", "NumberOfLogicalProcessors"])); }
        "mem"        => { hm.insert("mem".to_owned(), format!("{:.0}GB", get_wmic(&["os", "get", "TotalVisibleMemorySize"]).parse::<f64>().unwrap() / 1024.0 / 1024.0)); }
        "mb"         => { hm.insert("mb".to_owned(), get_wmic(&["baseboard", "get", "Product"])); }
        "mbvendor"   => { hm.insert("mbvendor".to_owned(), get_wmic(&["baseboard", "get", "Manufacturer"])); }
        "gpu"        => { hm.insert("gpu".to_owned(), get_wmic(&["path", "win32_videocontroller", "get", "Name"]).replace("\n", ",")); }
        "gpuvendor"  => { hm.insert("gpuvendor".to_owned(), get_wmic(&["path", "win32_videocontroller", "get", "AdapterCompatibility"]).replace("\n", ",")); }
        "drive"      => { hm.insert("drive".to_owned(), get_wmic(&["diskdrive", "get", "model"]).split("\n").map(|a| a.trim_end()).collect::<Vec<&str>>().join(",")); }
        "drivesize"  => drivesize(hm),
        _ => panic!()
    }
}

fn get_wmic(args: &[&str]) -> String {
    unsafe {
        let s = std::str::from_utf8_unchecked(
            &process::Command::new("wmic")
            .args(args)
            .output()
            .unwrap()
            .stdout
        ).replace("\r", "");
        s[s.find("\n").unwrap() + 1..].trim_end().to_owned()
    }
}

fn drivesize(hm: &mut HashMap<String, String>) {
    hm.insert("drivesize".to_owned(),
        get_wmic(&["diskdrive", "get", "size"])
        .split("\n")
        .map(|a| format!("{:.0}GB", a.trim().parse::<f64>().unwrap() / 1000.0 / 1000.0 / 1000.0))
        .collect::<Vec<String>>()
        .join(",")
    );
}
