use structopt::StructOpt;

#[derive(StructOpt)]
pub struct Opt {

    /// Tags to display.
    #[structopt()]
    pub tags: Vec<String>,

    /// Format to display fields in. Available: aligned, tabfile, json.
    #[structopt(long, default_value="aligned")]
    pub format: String,

}