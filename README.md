This is a WORK IN PROGRESS,
Do not expect too much from it!

# HARDI
This is HARDI, the HARDware Information cross-platform command-line tool.
It is used to examine hardware information. While it has an interactive command-line interface, HARDI is mostly used as a dependency and to provide information
for your own applications.

## Rust library
Writing a program in Rust and need HARDware Info? You're in luck! HARDI is (not yet) available as a Cargo crate!
Add the crate to your Cargo.toml dependencies. You know the drill!
For usage, open the gitlab repo and check the `examples` directory [(here)](https://gitlab.com/xamn/hardi/-/tree/master/examples).

The following code prints out the model name of your CPU.
```rust
fn main() {
    println!("{}", hardi::get_field("cpu").unwrap());
}
```

## Usage
To see all information you can simply run `hardi`.
You can also just specify the wanted field or fields.  
```
hardi cpu                   # display the model of your CPU

hardi                       # display all info

hardi cpu mb                # multiple fields

hardi --format json         # info in a json format
```

### Fields
|Field     | Explanation                   | Example                                |
|----------|-------------------------------|----------------------------------------|
|os        | Operating System Type         | linux                                  |
|family    | Operating System Family       | unix                                   |
|arch      | CPU Architecture              | x86_64                                 |
|distro    | Operating System Distribution | Arch Linux                             |
|cpu       | CPU Model Name                | AMD Ryzen 7 3700X 8-Core Processor     |
|cpuvendor | CPU Vendor                    | AuthenticAMD                           |
|cores     | CPU Core Count                | 8                                      |
|threads   | CPU Thread Count              | 16                                     |
|mem       | System Memory                 | 32GB                                   |
|mb        | Motherboard Model Name        | X570-A PRO (MS-7C37)                   |
|mbvendor  | Motherboard Vendor            | Micro-Star International Co., Ltd.     |
|gpu       | GPU Model Name(s)             | NVIDIA GeForce RTX 2070 SUPER          |
|gpuvendor | GPU Vendor(s)                 | NVIDIA                                 |
|drive     | Drive Model Name(s)           | KINGSTON SA2000M81000G,Samsung SSD 870 |
|drivesize | Drive Size(s)                 | 1000GB,1000GB                          |
