fn main() {
    let fields = hardi::get_fields(&["mb", "mbvendor"]).unwrap();
    println!("motherboard: {}, vendor: {}", fields.get("mb").unwrap(), fields.get("mbvendor").unwrap());
}