mod linux;
mod windows;
mod macos;

use std::collections::HashMap;

pub fn get_field(field: &str) -> Result<String, String> {
    let mut hm = HashMap::new();
    if let Err(err) = get_field_internal(field, &mut hm) {
        return Err(err);
    }
    Ok(hm[field].clone())
}

pub fn get_fields(fields: &[&str]) -> Result<HashMap<String, String>, String> {
    let mut hm = HashMap::new();
    let mut out_hm = HashMap::new();
    for &field in fields {
        get_field_internal(field, &mut hm)?;
        out_hm.insert(field.to_owned(), hm[field].clone());
    }
    Ok(out_hm)
}

fn get_field_internal(field: &str, hm: &mut HashMap<String, String>) -> Result<(), String> {
    match field {
        "os" =>     { hm.insert(field.to_owned(), std::env::consts::OS.to_owned()); Ok(()) },
        "family" => { hm.insert(field.to_owned(), std::env::consts::FAMILY.to_owned()); Ok(()) },
        "arch" =>   { hm.insert(field.to_owned(), std::env::consts::ARCH.to_owned()); Ok(()) },
        "distro" |
        "cpu" |
        "cpuvendor" |
        "cores" |
        "threads" |
        "mem" |
        "mb" |
        "mbvendor" |
        "gpu" |
        "gpuvendor" |
        "vram" |
        "drive" |
        "drivesize" => {
            if cfg!(target_os = "linux") {
                linux::get_field(field, hm);
            } else if cfg!(target_os = "windows") {
                windows::get_field(field, hm);
            } else if cfg!(target_os = "macos") {
                macos::get_field(field, hm);
            } else {
                return Err("platform unsupported".to_owned())
            }
            Ok(())
        }
        _ => Err(format!("invalid field `{}`", field))
    }
}
