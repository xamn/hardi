use std::{fs, process};
use std::collections::HashMap;
use std::path::PathBuf;

pub(super) fn get_field(field: &str, hm: &mut HashMap<String, String>) {
    if hm.contains_key(field) {
        return;
    }
    match field {
        "distro"      => distro(hm),
        "cpu"         |
        "cpuvendor"   |
        "cores"       |
        "threads"     => cpu(hm),
        "mem"         => mem(hm),
        "mb"          => mb(hm),
        "mbvendor"    => mbvendor(hm),
        "gpu"         => pci(hm),
        "gpuvendor"   => pci(hm),
        "drive"       => sys_drive(hm),
        "drivesize"   => sys_drive(hm),
        _ => panic!()
    }
}

fn distro(hm: &mut HashMap<String, String>) {
    for line in fs::read_to_string("/etc/os-release").unwrap().split("\n") {
        if line.starts_with("NAME") {
            hm.insert("distro".to_owned(), line["NAME=\"".len()..line.len() - 1].to_owned());
            return;
        }
    }
}

pub fn cpu(hm: &mut HashMap<String, String>) {
    if hm.contains_key("cpu") {
        return;
    }
    match std::str::from_utf8(&process::Command::new("lscpu").output().unwrap().stdout) {
        Ok(output) => {
            for line in output.split("\n") {
                let trimmed = line.trim();
                if trimmed.starts_with("Model name:") {
                    hm.insert("cpu".to_owned(), trimmed["Model name:".len()..].trim().to_owned());
                }
                if trimmed.starts_with("Vendor ID:") {
                    hm.insert("cpuvendor".to_owned(), trimmed["Vendor ID:".len()..].trim().to_owned());
                }
                if trimmed.starts_with("Core(s) per socket:") {
                    hm.insert("cores".to_owned(), trimmed["Core(s) per socket:".len()..].trim().to_owned());
                }
                if trimmed.starts_with("CPU(s):") {
                    hm.insert("threads".to_owned(), trimmed["CPU(s):".len()..].trim().to_owned());
                }
            }
        }
        Err(_) => ()
    }
}

fn mem(hm: &mut HashMap<String, String>) {
    for line in fs::read_to_string("/proc/meminfo").unwrap().split("\n") {
        if line.starts_with("MemTotal") {
            hm.insert("mem".to_owned(),format!("{:.0}GB", (line["MemTotal:".len()..line.len()-2].trim().parse::<f64>().unwrap() / 1000.0 / 1000.0).ceil()));
        }
    }
}

fn mb(hm: &mut HashMap<String, String>) {
    match fs::read_to_string("/sys/devices/virtual/dmi/id/board_name") {
        Ok(text) => hm.insert("mb".to_owned(), text.trim_end().to_owned()),
        Err(_) => hm.insert("mb".to_owned(), "N/A".to_owned()),
    };
}

fn mbvendor(hm: &mut HashMap<String, String>) {
    match fs::read_to_string("/sys/devices/virtual/dmi/id/board_vendor") {
        Ok(text) => hm.insert("mbvendor".to_owned(), text.trim_end().to_owned()),
        Err(_) => hm.insert("mbvendor".to_owned(), "N/A".to_owned()),
    };
}


fn pci(hm: &mut HashMap<String, String>) {
    if hm.contains_key("gpu") {
        return;
    }
    let out = std::str::from_utf8(
        &process::Command::new("lspci").output().unwrap().stdout).unwrap().to_owned();
    let mut gpu_models = vec![];
    let mut gpu_vendors = vec![];
    for line in out.split("\n") {
        if  line.contains("VGA compatible controller") ||
            line.contains("Display controller") ||
            line.contains("3D controller") {
            if let Some(i) = line.rfind("[") {
                gpu_models.push(line[i+1..line.rfind("]").unwrap()].to_owned());
            } else {
                gpu_models.push(line.replace(",", ""));
            }
            gpu_vendors.push(
                if line.contains("AMD") { "AMD" }
                else if line.contains("NVIDIA") { "NVIDIA" }
                else { "Intel" }
            )
        }
    }
    hm.insert("gpu".to_owned(), gpu_models.join(","));
    hm.insert("gpuvendor".to_owned(), gpu_vendors.join(","));
}

fn sys_drive(hm: &mut HashMap<String, String>) {
    if hm.contains_key("disk") {
        return;
    }
    let mut names = vec![];
    let mut models = vec![];
    let mut sizes = vec![];

    for drive in glob::glob("/sys/block/*").unwrap() {

        let name = drive.unwrap().iter().last().unwrap().to_str().unwrap().to_owned();
        let mut model_path = PathBuf::new();
        model_path.push("/sys/block");
        model_path.push(&name);
        model_path.push("device/model");

        // ignore drives without a model name
        if model_path.exists() {
            models.push(fs::read_to_string(model_path).unwrap().trim_end().to_owned());
            names.push(name);
        }
    }

    let lsblk_output = process::Command::new("lsblk").output().unwrap().stdout;
    let lsblk_str = unsafe { std::str::from_utf8_unchecked(&lsblk_output) };
    for name in &names {
        for line in lsblk_str.split("\n") {
            if line.starts_with(name) {
                let columns: Vec<&str> = line.split_ascii_whitespace().collect();
                let value: f64 = columns[3][0..columns[3].len() - 1].parse().unwrap();
                sizes.push(format!("{:.0}GB", value * 1.074));
                break;
            }
        }
    }

    hm.insert("drive".to_owned(), models.join(","));
    hm.insert("drivesize".to_owned(), sizes.join(","));
}
